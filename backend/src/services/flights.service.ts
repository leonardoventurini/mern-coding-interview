import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return FlightsModel.find({});
    }

    async setStatus(_id: string, status: string) {
        return FlightsModel.updateOne({_id }, { $set: { status}});
    }
}
