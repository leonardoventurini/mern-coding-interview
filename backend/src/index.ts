import 'reflect-metadata'
import { createExpressServer, RoutingControllersOptions, } from 'routing-controllers'
import { db } from './memory-database'

require('dotenv').config()

const port = process.env.PORT

const routingControllersOptions: RoutingControllersOptions = {
    routePrefix: '/v1',
    controllers: [`${__dirname}/controllers/*.controller.*`],
    validation: true,
    classTransformer: true,
    cors: true,
    defaultErrorHandler: true,
}

const app = createExpressServer(routingControllersOptions)

// Connect to In-Memory DB
;(async () => {
    await db({test: false});
})().catch(console.error)

app.listen(port, () => {
    console.log(`[Live Coding Challenge] Running at http://localhost:${port}`)
})

export default app
